package pro.hakta.kvaapp.business.apartment;

import java.util.List;

import pro.hakta.kvaapp.data.entity.apartment.FlatEntity;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import pro.hakta.kvaapp.data.repositories.IApartmentRepository;
import rx.Observable;

/**
 * Created by Yakovlev Andrey on 7/9/2017.
 */

public class ApartmentInteractor implements IApartmentInteractor {

    IApartmentRepository mApartmentRepository;

    public ApartmentInteractor(IApartmentRepository apartmentRepository) {
        mApartmentRepository = apartmentRepository;
    }

    @Override
    public Observable<List<FlatEntityRealm>> getApartments() {
        return mApartmentRepository.getApartments();
    }
}
