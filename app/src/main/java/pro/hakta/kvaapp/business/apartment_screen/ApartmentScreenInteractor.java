package pro.hakta.kvaapp.business.apartment_screen;

import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import pro.hakta.kvaapp.data.repositories.IApartmentRepository;
import rx.Observable;
import rx.Single;

/**
 * Created by Yakovlev Andrey.
 */

public class ApartmentScreenInteractor implements IApartmentScreenInteractor {

    private IApartmentRepository mApartmentRepository;

    public ApartmentScreenInteractor(IApartmentRepository apartmentRepository) {
        mApartmentRepository = apartmentRepository;
    }

    @Override
    public Observable<FlatEntityRealm> loadApartmentInfoById(String id) {
        return mApartmentRepository.getApartmentInfoById(id);
    }
}
