package pro.hakta.kvaapp.business.apartment;

import java.util.List;

import pro.hakta.kvaapp.data.entity.apartment.FlatEntity;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import rx.Observable;

/**
 * Created by Yakovlev Andrey on 7/9/2017.
 */

public interface IApartmentInteractor {
    Observable<List<FlatEntityRealm>> getApartments();
}
