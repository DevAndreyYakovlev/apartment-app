package pro.hakta.kvaapp.business.apartment_screen;

import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import rx.Observable;
import rx.Single;

/**
 * Created by Yakovlev Andrey.
 */

public interface IApartmentScreenInteractor {
    Observable<FlatEntityRealm> loadApartmentInfoById(String id);
}
