package pro.hakta.kvaapp.di.main;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

import io.realm.annotations.PrimaryKey;


@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MainScope {
}
