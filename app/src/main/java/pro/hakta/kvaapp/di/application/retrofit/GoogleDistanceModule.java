package pro.hakta.kvaapp.di.application.retrofit;

import android.annotation.SuppressLint;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import pro.hakta.kvaapp.utils.GoogleDistanceApiService;
import pro.hakta.kvaapp.utils.KvAppApiService;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yakovlev Andrey.
 */
@Module
public class GoogleDistanceModule {

    String mBaseUrl = "http://maps.googleapis.com";
    private Context application;
    private static final int TIMEOUT = 45;

    private static final String API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    @SuppressLint("SimpleDateFormat")
    private static DateFormat df = new SimpleDateFormat(API_DATE_FORMAT);

    @Provides
    Cache provideHttpCache(Context appContext) {
        int cacheSize = 10 * 1024 * 1024;
        this.application = appContext;

        return new Cache(appContext.getCacheDir(), cacheSize);
    }

    @Provides
    Gson provideGson() {
        return new GsonBuilder()
                .create();
    }

    @Provides
    OkHttpClient provideOkhttpClient(Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(cache)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS);
        return client.build();
    }

    @Provides
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .build();
    }

    @Provides
    GoogleDistanceApiService provideGoogleDistanceApi(Retrofit retrofit){
        return retrofit.create(GoogleDistanceApiService.class);
    }

}
