package pro.hakta.kvaapp.di.apartment_screen;

import dagger.Subcomponent;
import pro.hakta.kvaapp.ui.apartment_screen.ui.ApartmentActivity;

/**
 * Created by Yakovlev Andrey.
 */
@Subcomponent(modules = {ApartmentScreenModule.class})
public interface ApartmentScreenComponent {

    void inject(ApartmentActivity apartmentActivity);

}
