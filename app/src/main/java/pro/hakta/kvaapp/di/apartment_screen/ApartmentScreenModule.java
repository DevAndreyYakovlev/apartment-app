package pro.hakta.kvaapp.di.apartment_screen;

import dagger.Module;
import dagger.Provides;
import pro.hakta.kvaapp.business.apartment_screen.ApartmentScreenInteractor;
import pro.hakta.kvaapp.business.apartment_screen.IApartmentScreenInteractor;
import pro.hakta.kvaapp.data.repositories.IApartmentRepository;
import pro.hakta.kvaapp.ui.apartment_screen.presenter.ApartmentScreenPresenter;
import pro.hakta.kvaapp.ui.apartment_screen.presenter.IApartmentScreenPresenter;

/**
 * Created by Yakovlev Andrey.
 */
@Module
public class ApartmentScreenModule {

    @Provides
    IApartmentScreenPresenter provideApartmentScreenPresenter(IApartmentScreenInteractor apartmentScreenInteractor){
        return new ApartmentScreenPresenter(apartmentScreenInteractor);
    }

    @Provides
    IApartmentScreenInteractor provideApartmentScreenInteractor(IApartmentRepository apartmentRepository){
        return new ApartmentScreenInteractor(apartmentRepository);
    }

}
