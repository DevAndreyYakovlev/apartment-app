package pro.hakta.kvaapp.di.apartment_list;

import javax.inject.Singleton;

import dagger.Subcomponent;
import pro.hakta.kvaapp.di.apartment.ApartmentModule;
import pro.hakta.kvaapp.ui.apartment_list.view.ApartmentListFragment;

/**
 * Created by Yakovlev Andrey.
 */
@Subcomponent(modules = {ApartmentListModule.class})
public interface ApartmentListComponent {
    void inject(ApartmentListFragment apartmentListFragment);
}
