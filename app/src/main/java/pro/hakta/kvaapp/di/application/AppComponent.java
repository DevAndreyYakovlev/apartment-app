package pro.hakta.kvaapp.di.application;

import javax.inject.Singleton;

import dagger.Component;
import pro.hakta.kvaapp.di.apartment.ApartmentModule;
import pro.hakta.kvaapp.di.apartment_list.ApartmentListComponent;
import pro.hakta.kvaapp.di.apartment_list.ApartmentListModule;
import pro.hakta.kvaapp.di.apartment_screen.ApartmentScreenComponent;
import pro.hakta.kvaapp.di.apartment_screen.ApartmentScreenModule;
import pro.hakta.kvaapp.di.application.retrofit.GoogleDistanceModule;
import pro.hakta.kvaapp.di.application.retrofit.KvAppRetrofitModule;
import pro.hakta.kvaapp.di.main.MainComponent;
import pro.hakta.kvaapp.di.main.MainModule;


@Component(modules = {AppModule.class, KvAppRetrofitModule.class, ApartmentModule.class})
@Singleton
public interface AppComponent {

    MainComponent plus(MainModule mainModule);
    ApartmentListComponent plus(ApartmentListModule apartmentListModule);
    ApartmentScreenComponent plus(ApartmentScreenModule apartmentScreenModule);

}
