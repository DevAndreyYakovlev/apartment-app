package pro.hakta.kvaapp.di.main;

import dagger.Module;
import dagger.Provides;
import pro.hakta.kvaapp.ui.main.presenter.IMainPresenter;
import pro.hakta.kvaapp.ui.main.presenter.MainPresenter;


@Module
public class MainModule {

    @Provides
    @MainScope
    IMainPresenter provideMainPresenter(){
        return new MainPresenter();
    }

}
