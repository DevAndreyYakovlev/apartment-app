package pro.hakta.kvaapp.di.apartment_list;

import dagger.Module;
import dagger.Provides;
import pro.hakta.kvaapp.business.apartment.ApartmentInteractor;
import pro.hakta.kvaapp.business.apartment.IApartmentInteractor;
import pro.hakta.kvaapp.ui.apartment_list.presenter.ApartmentListPresenter;
import pro.hakta.kvaapp.ui.apartment_list.presenter.IApartmentListPresenter;

/**
 * Created by Yakovlev Andrey.
 */
@Module
public class ApartmentListModule {

    @Provides
    IApartmentListPresenter provideApartmentListPresenter(IApartmentInteractor apartmentInteractor){
        return new ApartmentListPresenter(apartmentInteractor);
    }

}
