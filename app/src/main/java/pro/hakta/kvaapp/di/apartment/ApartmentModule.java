package pro.hakta.kvaapp.di.apartment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pro.hakta.kvaapp.business.apartment.ApartmentInteractor;
import pro.hakta.kvaapp.business.apartment.IApartmentInteractor;
import pro.hakta.kvaapp.data.repositories.ApartmentRepository;
import pro.hakta.kvaapp.data.repositories.IApartmentRepository;
import pro.hakta.kvaapp.utils.KvAppApiService;
import pro.hakta.kvaapp.utils.RealmProvider;

/**
 * Created by Yakovlev Andrey.
 */
@Module
public class ApartmentModule {

    @Provides
    @Singleton
    IApartmentInteractor provideApartmentInteractor(IApartmentRepository apartmentRepository){
        return new ApartmentInteractor(apartmentRepository);
    }

    @Provides
    @Singleton
    IApartmentRepository provideApartmentRepository(KvAppApiService kvAppApiService, RealmProvider realmProvider){
        return new ApartmentRepository(kvAppApiService, realmProvider);
    }

}
