package pro.hakta.kvaapp.di.main;

import dagger.Subcomponent;
import pro.hakta.kvaapp.ui.main.views.MainActivity;


@Subcomponent(modules = {MainModule.class})
@MainScope
public interface MainComponent {

    void inject(MainActivity mainActivity);

}
