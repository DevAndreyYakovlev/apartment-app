package pro.hakta.kvaapp.di.apartment_list;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Yakovlev Andrey.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ApartmentListScope {
}
