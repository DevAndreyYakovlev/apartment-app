package pro.hakta.kvaapp.data.repositories;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntity;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import pro.hakta.kvaapp.data.entity.apartment.Photo;
import pro.hakta.kvaapp.utils.BaseResponse;
import pro.hakta.kvaapp.utils.GoogleDistanceApi;
import pro.hakta.kvaapp.utils.GoogleDistanceApiService;
import pro.hakta.kvaapp.utils.KvAppApiService;
import pro.hakta.kvaapp.utils.RealmProvider;
import rx.Observable;
import rx.Single;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Yakovlev Andrey.
 */

public class ApartmentRepository implements IApartmentRepository {

    private KvAppApiService mKvAppApiService;
    private RealmProvider mRealmProvider;

    public ApartmentRepository(KvAppApiService kvAppApiService, RealmProvider realmProvider) {
        mKvAppApiService = kvAppApiService;
        mRealmProvider = realmProvider;
    }

    @Override
    public Observable<List<FlatEntityRealm>> getApartments() {

        if (!checkRealmData()){
            return loadAndSaveApartmentList();
        }

        return getApartmentFromRealm();
    }

    private Observable<List<FlatEntityRealm>> loadAndSaveApartmentList(){
        GoogleDistanceApiService googleDistanceApiService = GoogleDistanceApi.getDistance();

        return mKvAppApiService.getFlats()
                .map(BaseResponse::getData)
                .flatMap(Observable::from)
                .filter(flatEntity -> flatEntity.getLongitude() !=0 && flatEntity.getLatitude() != 0)
                .flatMap(flatEntity -> {
                    Map<String,String> json = new HashMap<>();
                    json.put("origins","55.825070,49.135506");
                    json.put("destinations",flatEntity.getLatitude() + "," + flatEntity.getLongitude());
                    return googleDistanceApiService.getDistance(json);
                }, (flatEntity, googleDistanceEntity) -> {
                    flatEntity.setDistance(googleDistanceEntity.getRows().get(0).getElements().get(0).getDistance().getText());
                    return flatEntity;
                })
                .map(this::saveApartmentToRealm)
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private FlatEntityRealm saveApartmentToRealm(FlatEntity flatEntity){
        Realm realm = mRealmProvider.provideRealm();
        realm.beginTransaction();
        FlatEntityRealm flatEntityRealm = castFlatEntityToRealm(flatEntity);
        realm.insertOrUpdate(flatEntityRealm);
        realm.commitTransaction();
        realm.close();
        return flatEntityRealm;
    }

    private FlatEntityRealm castFlatEntityToRealm(FlatEntity flatEntity){
        FlatEntityRealm flatEntityRealm = new FlatEntityRealm();
        flatEntityRealm.setDistance(flatEntity.getDistance());
        flatEntityRealm.setCreate_date(flatEntity.getCreate_date());
        flatEntityRealm.setId(flatEntity.getId());
        flatEntityRealm.setIs_favorite(flatEntity.getIs_favorite());
        flatEntityRealm.setIs_premium(flatEntity.getIs_premium());
        flatEntityRealm.setLatitude(flatEntity.getLatitude());
        flatEntityRealm.setLongitude(flatEntity.getLongitude());
        flatEntityRealm.setPhone_number(flatEntity.getPhone_number());
        flatEntityRealm.setPrice_day(flatEntity.getPrice_day());
        flatEntityRealm.setPrice_hour(flatEntity.getPrice_hour());
        flatEntityRealm.setTitle(flatEntity.getTitle());
        flatEntityRealm.setUpdate_date(flatEntity.getUpdate_date());
        RealmList<Photo> realmListPhoto = new RealmList<>();
        realmListPhoto.addAll(flatEntity.getPhotos());
        flatEntityRealm.setPhotos(realmListPhoto);
        return flatEntityRealm;
    }

    @NonNull
    private Observable<List<FlatEntityRealm>> getApartmentFromRealm(){
        Realm realm = mRealmProvider.provideRealm();
        return Observable.just(realm.copyFromRealm(realm.where(FlatEntityRealm.class)
                .findAll()));
    }

    private boolean checkRealmData(){
        Realm realm = mRealmProvider.provideRealm();
        return realm.where(FlatEntityRealm.class).count() > 0;

    }

    @Override
    public Observable<FlatEntityRealm> getApartmentInfoById(String id) {

        Realm realm = mRealmProvider.provideRealm();

        return Observable.just(realm.copyFromRealm(realm.where(FlatEntityRealm.class).equalTo("id", id).findFirst()));
    }
}
