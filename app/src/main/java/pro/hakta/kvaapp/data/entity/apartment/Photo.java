package pro.hakta.kvaapp.data.entity.apartment;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by Yakovlev Andrey.
 */

public class Photo extends RealmObject {

    @SerializedName("id")
    private String id;

    @SerializedName("image")
    private String image;

    @SerializedName("preview")
    private String preview;

    @SerializedName("is_main")
    private String isMain;

    @SerializedName("position")
    private int position;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getIsMain() {
        return isMain;
    }

    public void setIsMain(String isMain) {
        this.isMain = isMain;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
