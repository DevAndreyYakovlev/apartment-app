package pro.hakta.kvaapp.data.entity.google_distance;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Yakovlev Andrey.
 */

public class Row {
    @SerializedName("elements")
    private List<Element> elements;

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }
}
