package pro.hakta.kvaapp.data.entity.apartment;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Yakovlev Andrey.
 */

public class FlatEntity {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("price_day")
    private double price_day;

    @SerializedName("price_hour")
    private double price_hour;

    @SerializedName("create_date")
    private String create_date;

    @SerializedName("update_date")
    private String update_date;

    @SerializedName("is_premium")
    private short is_premium;

    @SerializedName("is_favorite")
    private short is_favorite;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("phone_number")
    private String phone_number;

    private String distance;

    @SerializedName("photo")
    private ArrayList<Photo> photos;

    //region accessors
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice_day() {
        return price_day;
    }

    public void setPrice_day(double price_day) {
        this.price_day = price_day;
    }

    public double getPrice_hour() {
        return price_hour;
    }

    public void setPrice_hour(double price_hour) {
        this.price_hour = price_hour;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

    public short getIs_premium() {
        return is_premium;
    }

    public void setIs_premium(short is_premium) {
        this.is_premium = is_premium;
    }

    public short getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(short is_favorite) {
        this.is_favorite = is_favorite;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }

    //endregion


}
