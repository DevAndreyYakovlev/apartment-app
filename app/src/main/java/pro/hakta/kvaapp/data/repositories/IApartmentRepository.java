package pro.hakta.kvaapp.data.repositories;

import java.util.List;

import pro.hakta.kvaapp.data.entity.apartment.FlatEntity;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import rx.Observable;
import rx.Single;

/**
 * Created by Yakovlev Andrey.
 */

public interface IApartmentRepository {
    Observable<List<FlatEntityRealm>> getApartments();

    Observable<FlatEntityRealm> getApartmentInfoById(String id);
}
