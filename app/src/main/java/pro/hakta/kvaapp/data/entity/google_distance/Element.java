package pro.hakta.kvaapp.data.entity.google_distance;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yakovlev Andrey.
 */

public class Element {

    @SerializedName("distance")
    private Distance distance;
    @SerializedName("duration")
    private Duration duration;
    @SerializedName("status")
    private String status;

    public String test;

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
