package pro.hakta.kvaapp;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import pro.hakta.kvaapp.di.application.AppComponent;
import pro.hakta.kvaapp.di.application.AppModule;
import pro.hakta.kvaapp.di.application.DaggerAppComponent;

/**
 * Created by Yakovlev Andrey.
 */

public class KvApplication extends Application {

    @SuppressWarnings("NullableProblems")
    @NonNull
    private AppComponent appComponent;

    public static KvApplication get(@NonNull Context context){
        return (KvApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = prepareAppComponent().build();
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(realmConfig);
    }


    @NonNull
    private DaggerAppComponent.Builder prepareAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this));
    }

    @NonNull
    public AppComponent applicationComponent() {
        return appComponent;
    }

}
