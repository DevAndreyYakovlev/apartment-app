package pro.hakta.kvaapp.utils;

import io.realm.RealmObject;


public class RealmString extends RealmObject{

    public String val;

    public RealmString(String s) {
        val = s;
    }

    public RealmString(){

    }

}
