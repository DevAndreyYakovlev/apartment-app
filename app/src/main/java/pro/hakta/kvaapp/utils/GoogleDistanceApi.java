package pro.hakta.kvaapp.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * Created by Yakovlev Andrey on 4/8/2017.
 */

public class GoogleDistanceApi {

    private static final int TIMEOUT = 45;
    private static final Gson gson = new GsonBuilder().create();

    private static RxJavaCallAdapterFactory rxJavaCallAdapterFactory = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

    static Retrofit getRetrofit(){
        return new Retrofit.Builder()
                .baseUrl("http://maps.googleapis.com")
                .client(getHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
    }

    public static GoogleDistanceApiService getDistance(){
        return getRetrofit().create(GoogleDistanceApiService.class);
    }

    private static OkHttpClient getHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT,TimeUnit.SECONDS)
                .readTimeout(TIMEOUT,TimeUnit.SECONDS)
                .build();
    }

}
