package pro.hakta.kvaapp.utils;

import java.util.Map;

import pro.hakta.kvaapp.data.entity.google_distance.GoogleDistanceEntity;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by Yakovlev Andrey.
 */

public interface GoogleDistanceApiService {

    @GET("/maps/api/distancematrix/json")
    Observable<GoogleDistanceEntity> getDistance(@QueryMap Map<String,String> json);

}
