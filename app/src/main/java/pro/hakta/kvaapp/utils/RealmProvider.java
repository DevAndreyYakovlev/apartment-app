package pro.hakta.kvaapp.utils;

import io.realm.Realm;

/**
 * Created by Yakovlev Andrey.
 */

public class RealmProvider {

    public RealmProvider() {
    }

    public Realm provideRealm(){
        return Realm.getDefaultInstance();
    }

}
