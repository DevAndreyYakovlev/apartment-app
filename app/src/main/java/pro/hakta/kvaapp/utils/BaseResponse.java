package pro.hakta.kvaapp.utils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yakovlev Andrey.
 */

public class BaseResponse<T> {

    @SerializedName("data")
    private T data;
    @SerializedName("success")
    private String success;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
