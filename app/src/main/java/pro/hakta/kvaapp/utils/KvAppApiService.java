package pro.hakta.kvaapp.utils;

import java.util.ArrayList;

import pro.hakta.kvaapp.data.entity.apartment.FlatEntity;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Yakovlev Andrey on 4/3/2017.
 */

public interface KvAppApiService {

    @GET("/api/flat")
    Observable<BaseResponse<ArrayList<FlatEntity>>> getFlats();

}
