package pro.hakta.kvaapp.ui.main.views;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.hakta.kvaapp.KvApplication;
import pro.hakta.kvaapp.R;
import pro.hakta.kvaapp.di.main.MainModule;
import pro.hakta.kvaapp.ui.apartment_list.view.ApartmentListFragment;
import pro.hakta.kvaapp.ui.apartment_map.view.ApartmentMapFragment;
import pro.hakta.kvaapp.ui.main.presenter.IMainPresenter;

public class MainActivity extends AppCompatActivity implements IMainView {

    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;
    @Inject
    IMainPresenter mMainPresenter;
    private FragmentTransaction mFragmentTransaction;
    private FragmentManager mFragmentManager;
    private ApartmentListFragment mApartmentListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        KvApplication.get(getApplicationContext()).applicationComponent().plus(new MainModule()).inject(this);
        mFragmentManager = getSupportFragmentManager();
        mMainPresenter.bindView(this);
        mApartmentListFragment = new ApartmentListFragment();

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mMainPresenter.tabClick(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.add(R.id.fmt_container, mApartmentListFragment, "listFragment");
        mFragmentTransaction.commit();
    }


    @Override
    public void showListScreen() {
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (mFragmentManager.findFragmentByTag("listFragment") != null){
            return;
        }
        mFragmentTransaction.replace(R.id.fmt_container, mApartmentListFragment, "listFragment");
        mFragmentTransaction.commit();

    }

    @Override
    public void showMapScreen() {
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (mFragmentManager.findFragmentByTag("mapFragment") != null){
            return;
        }
        mFragmentTransaction.replace(R.id.fmt_container, new ApartmentMapFragment(), "mapFragment");
        mFragmentTransaction.commit();
    }

    @Override
    public void showSnackBar(String message) {

    }
}
