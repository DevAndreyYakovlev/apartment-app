package pro.hakta.kvaapp.ui.apartment_map.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pro.hakta.kvaapp.R;

/**
 * Created by Yakovlev Andrey.
 */

public class ApartmentMapFragment extends Fragment implements IApartmentMapView {

    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.apartment_map, container, false);



        return view;
    }
}
