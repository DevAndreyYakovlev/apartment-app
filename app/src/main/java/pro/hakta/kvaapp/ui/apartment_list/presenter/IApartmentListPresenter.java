package pro.hakta.kvaapp.ui.apartment_list.presenter;

import pro.hakta.kvaapp.ui.apartment_list.view.IApartmentListView;

/**
 * Created by Yakovlev Andrey on 7/9/2017.
 */

public interface IApartmentListPresenter {

    void loadApartments();
    void bindView(IApartmentListView apartmentListView);
    void unbindView();

}
