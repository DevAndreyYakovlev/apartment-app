package pro.hakta.kvaapp.ui.apartment_screen.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.hakta.kvaapp.R;
import pro.hakta.kvaapp.data.entity.apartment.Photo;

/**
 * Created by Yakovlev Andrey.
 */

public class ApartmentViewPagerItem extends Fragment {

    private View mRootView;
    private Photo mPhoto;
    @BindView(R.id.flatSlideImg) ImageView mApartmentSlideImage;

    @SuppressLint("ValidFragment")
    public ApartmentViewPagerItem(Photo photo) {
        mPhoto = photo;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.apartment_view_pager_item, container, false);
        ButterKnife.bind(this, mRootView);
        Glide.with(this).load(mPhoto.getImage()).into(mApartmentSlideImage);
        return mRootView;
    }
}
