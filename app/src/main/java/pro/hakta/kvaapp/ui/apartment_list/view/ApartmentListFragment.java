package pro.hakta.kvaapp.ui.apartment_list.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.hakta.kvaapp.KvApplication;
import pro.hakta.kvaapp.R;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntity;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import pro.hakta.kvaapp.di.apartment_list.ApartmentListModule;
import pro.hakta.kvaapp.ui.apartment_list.presenter.IApartmentListPresenter;
import pro.hakta.kvaapp.ui.apartment_screen.ui.ApartmentActivity;
import rx.Observable;

/**
 * Created by Yakovlev Andrey on 7/9/2017.
 */

public class ApartmentListFragment extends Fragment implements IApartmentListView {

    private View mRootView;
    @BindView(R.id.progressBarContainer) FrameLayout mProgressBarContainer;
    @Inject IApartmentListPresenter mApartmentListPresenter;
    @BindView(R.id.apartmentListRecyclerView) RecyclerView mApartmentList;
    ApartmentRecyclerViewAdapter mApartmentRecyclerViewAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        KvApplication.get(getContext()).applicationComponent().plus(new ApartmentListModule()).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.apartment_list, container, false);
        ButterKnife.bind(this, mRootView);
        initApartmentList();
        mApartmentListPresenter.bindView(this);
        mApartmentListPresenter.loadApartments();

        return mRootView;
    }

    private void initApartmentList() {
        mApartmentRecyclerViewAdapter = new ApartmentRecyclerViewAdapter();
        mApartmentList.setLayoutManager(new LinearLayoutManager(getContext()));
        mApartmentList.setAdapter(mApartmentRecyclerViewAdapter);
    }

    @Override
    public void showApartmentList(List<FlatEntityRealm> entities) {
        mApartmentRecyclerViewAdapter.addApartment(entities);
    }

    @Override
    public void showProgressBar() {
        mProgressBarContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        mProgressBarContainer.setVisibility(View.GONE);
    }

    @Override
    public void showSnackBarMessage(String message) {
//        Snackbar.make(mRootView.findViewById(R.id.listCoordinatorLayout), message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public Observable<FlatEntityRealm> getListItemOnClickObserver() {
        return mApartmentRecyclerViewAdapter.getFlatEntityItemOnClickObserver();
    }

    @Override
    public void openApartmentWindow(FlatEntityRealm entity) {
        Intent intent = new Intent(getContext(), ApartmentActivity.class);
        intent.putExtra("flatEntityId", entity.getId());
        getContext().startActivity(intent);
    }
}
