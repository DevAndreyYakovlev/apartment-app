package pro.hakta.kvaapp.ui.apartment_screen.ui;

import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;

/**
 * Created by Yakovlev Andrey.
 */

public interface IApartmentView {

    void showApartmentInfo(FlatEntityRealm flatEntityRealm);


}
