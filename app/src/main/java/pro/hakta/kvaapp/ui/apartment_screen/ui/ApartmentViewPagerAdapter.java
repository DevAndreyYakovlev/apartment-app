package pro.hakta.kvaapp.ui.apartment_screen.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import io.realm.RealmList;
import pro.hakta.kvaapp.data.entity.apartment.Photo;

/**
 * Created by Yakovlev Andrey.
 */

public class ApartmentViewPagerAdapter extends FragmentStatePagerAdapter {

    private RealmList<Photo> mPhotos;

    public ApartmentViewPagerAdapter(FragmentManager fm) {
        super(fm);
        mPhotos = new RealmList<>();
    }

    public void addPhotos(RealmList<Photo> photos){
        mPhotos.addAll(photos);
        this.notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return new ApartmentViewPagerItem(mPhotos.get(position));
    }

    @Override
    public int getCount() {
        return mPhotos.size();
    }
}
