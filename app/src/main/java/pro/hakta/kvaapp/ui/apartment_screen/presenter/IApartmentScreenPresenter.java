package pro.hakta.kvaapp.ui.apartment_screen.presenter;

import pro.hakta.kvaapp.ui.apartment_screen.ui.IApartmentView;

/**
 * Created by Yakovlev Andrey.
 */

public interface IApartmentScreenPresenter {

    void subscribe(IApartmentView apartmentView);
    void unSubscribe();
    void loadApartmentInfoById(String id);

}
