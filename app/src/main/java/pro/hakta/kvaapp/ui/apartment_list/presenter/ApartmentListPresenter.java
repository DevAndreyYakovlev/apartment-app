package pro.hakta.kvaapp.ui.apartment_list.presenter;


import java.util.List;

import pro.hakta.kvaapp.business.apartment.IApartmentInteractor;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntity;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import pro.hakta.kvaapp.ui.apartment_list.view.IApartmentListView;
import rx.Subscriber;

public class ApartmentListPresenter implements IApartmentListPresenter {

    private IApartmentInteractor mApartmentInteractor;
    private IApartmentListView mApartmentListView;

    public ApartmentListPresenter(IApartmentInteractor apartmentInteractor) {
        mApartmentInteractor = apartmentInteractor;
    }

    private void init() {
        mApartmentListView.getListItemOnClickObserver()
                .subscribe(entity -> {
                    mApartmentListView.openApartmentWindow(entity);
                });
    }


    @Override
    public void loadApartments() {
        mApartmentListView.showProgressBar();
        mApartmentInteractor
                .getApartments()
                .subscribe(this::successLoad, this::errorLoad);
    }

    private void successLoad(List<FlatEntityRealm> entities) {
        mApartmentListView.showApartmentList(entities);
        mApartmentListView.hideProgressBar();
        mApartmentListView.showSnackBarMessage("Список был обнавлен");
    }

    private void errorLoad(Throwable throwable){
        
    }


    @Override
    public void bindView(IApartmentListView apartmentListView) {
        mApartmentListView = apartmentListView;
        init();
    }

    @Override
    public void unbindView() {
        mApartmentListView = null;
    }



}
