package pro.hakta.kvaapp.ui.main.presenter;


import android.support.design.widget.TabLayout;

import pro.hakta.kvaapp.ui.main.views.IMainView;

public interface IMainPresenter {

    void bindView(IMainView mainView);
    void unbindView();
    void tabClick(TabLayout.Tab tab);

}
