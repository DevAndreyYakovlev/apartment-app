package pro.hakta.kvaapp.ui.apartment_screen.presenter;

import pro.hakta.kvaapp.business.apartment_screen.IApartmentScreenInteractor;
import pro.hakta.kvaapp.ui.apartment_screen.ui.IApartmentView;

/**
 * Created by Yakovlev Andrey.
 */

public class ApartmentScreenPresenter implements IApartmentScreenPresenter {

    private IApartmentScreenInteractor mApartmentScreenInteractor;
    private IApartmentView mApartmentView;

    public ApartmentScreenPresenter(IApartmentScreenInteractor apartmentScreenInteractor) {
        mApartmentScreenInteractor = apartmentScreenInteractor;
    }

    @Override
    public void subscribe(IApartmentView apartmentView) {
        mApartmentView = apartmentView;
    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void loadApartmentInfoById(String id) {
        mApartmentScreenInteractor.loadApartmentInfoById(id).subscribe(item -> {
            mApartmentView.showApartmentInfo(item);
        });
    }
}
