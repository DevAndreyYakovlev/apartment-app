package pro.hakta.kvaapp.ui.main.presenter;


import android.support.design.widget.TabLayout;

import pro.hakta.kvaapp.ui.main.views.IMainView;

public class MainPresenter implements IMainPresenter {

    private IMainView mMainView;

    @Override
    public void bindView(IMainView mainView) {
        mMainView = mainView;
        mMainView.showSnackBar("");
    }

    @Override
    public void unbindView() {
        mMainView = null;
    }

    @Override
    public void tabClick(TabLayout.Tab tab) {
        if (tab.getPosition() == 0){
            mMainView.showListScreen();
        }else{
            mMainView.showMapScreen();
        }
    }


}
