package pro.hakta.kvaapp.ui.apartment_list.view;

import java.util.List;

import pro.hakta.kvaapp.data.entity.apartment.FlatEntity;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import rx.Observable;

/**
 * Created by Yakovlev Andrey on 7/9/2017.
 */

public interface IApartmentListView {

    void showApartmentList(List<FlatEntityRealm> entities);
    void showProgressBar();
    void hideProgressBar();
    void showSnackBarMessage(String message);
    Observable<FlatEntityRealm> getListItemOnClickObserver();

    void openApartmentWindow(FlatEntityRealm entity);
}
