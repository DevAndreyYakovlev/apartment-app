package pro.hakta.kvaapp.ui.apartment_screen.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.hakta.kvaapp.KvApplication;
import pro.hakta.kvaapp.R;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import pro.hakta.kvaapp.di.apartment_screen.ApartmentScreenModule;
import pro.hakta.kvaapp.ui.apartment_screen.presenter.IApartmentScreenPresenter;

/**
 * Created by Yakovlev Andrey.
 */

public class ApartmentActivity extends AppCompatActivity implements IApartmentView {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.flatStreetName) TextView mStreetName;
    @BindView(R.id.flatDistance) TextView mFlatDistance;
    @BindView(R.id.flatWorkTime) TextView mFlatWorkTime;
    @BindView(R.id.flatCost) TextView mFlatCost;
    @BindView(R.id.flatPhoneNumber) TextView mFlatPhoneNumber;
    @BindView(R.id.flatViewPager) ViewPager mFlatViewPager;
    private ApartmentViewPagerAdapter mApartmentViewPagerAdapter;


    @Inject
    IApartmentScreenPresenter mApartmentScreenPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apartment);
        ButterKnife.bind(this);
        KvApplication.get(getApplicationContext()).applicationComponent().plus(new ApartmentScreenModule()).inject(this);
        mApartmentScreenPresenter.subscribe(this);
        String id = (String) getIntent().getExtras().get("flatEntityId");
        mApartmentScreenPresenter.loadApartmentInfoById(id);

    }

    private void initToolBar(String title) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {finish();});
    }

    @Override
    public void showApartmentInfo(FlatEntityRealm flatEntity) {
        mApartmentViewPagerAdapter = new ApartmentViewPagerAdapter(getSupportFragmentManager());
        mFlatViewPager.setAdapter(mApartmentViewPagerAdapter);
        initToolBar(flatEntity.getTitle());
        mStreetName.setText("ул. "+flatEntity.getTitle());
        mFlatDistance.setText(flatEntity.getDistance() );
        int price = (int) flatEntity.getPrice_day();
        mFlatCost.setText("от " + price + " р./сут.");
        mFlatPhoneNumber.setText(flatEntity.getPhone_number());
        mApartmentViewPagerAdapter.addPhotos(flatEntity.getPhotos());

    }
}
