package pro.hakta.kvaapp.ui.apartment_list.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pro.hakta.kvaapp.R;
import pro.hakta.kvaapp.data.entity.apartment.FlatEntityRealm.FlatEntityRealm;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by Yakovlev Andrey.
 */

public class ApartmentRecyclerViewAdapter extends RecyclerView.Adapter<ApartmentRecyclerViewAdapter.MyViewHolder> {

    private List<FlatEntityRealm> mFlatEntityRealm;

    public ApartmentRecyclerViewAdapter(){
        mFlatEntityRealm = new ArrayList<>();
    }

    private PublishSubject<FlatEntityRealm> listItemOnClickSubject = PublishSubject.create();

    public void addApartment(List<FlatEntityRealm> entityRealmList){
        mFlatEntityRealm.addAll(entityRealmList);
        notifyDataSetChanged();
    }

    Observable<FlatEntityRealm> getFlatEntityItemOnClickObserver(){
        return listItemOnClickSubject.asObservable();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.apartment_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FlatEntityRealm flatEntityRealm = mFlatEntityRealm.get(position);
        holder.itemTitle.setText(flatEntityRealm.getTitle());
        holder.itemDistance.setText(flatEntityRealm.getDistance());
        holder.mListItemRoot.setOnClickListener(v -> listItemOnClickSubject.onNext(flatEntityRealm));
    }

    @Override
    public int getItemCount() {
        return mFlatEntityRealm.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.listItemTitle) TextView itemTitle;
        @BindView(R.id.listItemDistance) TextView itemDistance;
        @BindView(R.id.listItemRootView) LinearLayout mListItemRoot;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
