package pro.hakta.kvaapp.ui.main.views;



public interface IMainView {

    void showListScreen();
    void showMapScreen();
    void showSnackBar(String message);

}
